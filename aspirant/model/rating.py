import sqlalchemy as sa
from formbar.form import Form
from ringo.lib.form import get_form_config
from ringo.model import Base
from ringo.model.base import BaseItem
from ringo.model.mixins import (
    Owned,
    StateMixin
)
from ringo.model.statemachine import (
    State,
    Statemachine,
    null_condition as condition,
    null_handler as handler
)


desc_s1 = """S1_DESC"""
desc_s2 = """S2_DESC"""


def finish_condition(item, transition):
    """Only ratings which are filled out completly can be finished"""
    old_state = transition.get_start()
    request = old_state._statemachine._request
    formconfig = get_form_config(item, "update")
    form = Form(formconfig, item, dbsession=request.db)
    form.validate(None)
    return not (form.has_warnings() or form.has_errors())


class RatingStatemachine(Statemachine):

    def setup(self):
        _ = lambda x: x
        s1 = State(self, 1, _("In Progress"),
                   description=_(desc_s1))
        s2 = State(self, 2, _("Finished"),
                   description=_(desc_s2))

        s1.add_transition(s2, _("Finish rating"), handler, finish_condition)
        s2.add_transition(s1, _("Edit rating"), handler, condition)
        return s1


class RatingStateMixin(StateMixin):

    _statemachines = {"rating_state_id": RatingStatemachine}

    rating_state_id = sa.Column(sa.Integer, default=1)

    @property
    def rating_state(self):
        state = self.get_statemachine('rating_state_id')
        return state.get_state()


class Rating(BaseItem, RatingStateMixin, Owned, Base):
    __tablename__ = 'ratings'
    _modul_id = 1001
    id = sa.Column(sa.Integer, primary_key=True)
    application_id = sa.Column(sa.Integer,
                               sa.ForeignKey("applications.id"))

    support_needed = sa.Column('support_needed', sa.Integer)
    support_comment = sa.Column('support_comment', sa.String, nullable=False,
                                server_default='')

    application = sa.orm.relation("Application")

    def get_values(self, include_relations=False, serialized=False):
        """Overwritten get_values method which adds the points of the rating."""
        values = BaseItem.get_values(self, include_relations, serialized)
        values["points"] = self.points
        return values

    def get_rating_fields(self):
        """Will return a list of fieldnames which are tagged with the
        tag 'rating'. Please note that this mthod is the default
        behaviour which will exime the form to get the fields which is
        very expensive. You will very linkly want to reimplement this
        method in your application and provide some kind of cache are
        static values."""
        form_config = get_form_config(self, "update")
        form = Form(form_config, self)
        values = form.deserialize(form.loaded_data)
        fields = form._config.get_fields(values=values,
                                         evaluate=False)
        fieldnames = []
        for field in fields.items():
            if "rating" in field[1].tags:
                fieldnames.append(field[0])
        return fieldnames

    @property
    def points(self):
        """Property which implements the rating fucntionallity. This
        logic is custom to the application. So this methid needs to be
        very likly overwritten in the application. The default behavior
        is so sum up all values of all enabled fields of the ratings
        form which are tagged with tha tag 'rating'."""
        points = 0
        for field in self.get_rating_fields():
            value = self.get_value(field)
            if value:
                points += value
        return points
