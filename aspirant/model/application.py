import sqlalchemy as sa
from formbar.form import Form
from ringo.model import Base
from ringo.model.base import BaseItem
from ringo.model.mixins import (
    Owned,
    Meta,
    StateMixin
)
from ringo.model.statemachine import (
    State,
    Statemachine,
    null_condition as condition,
    null_handler as handler
)
from ringo.lib.helpers import get_item_modul
from ringo.lib.message import Mail, Mailer
from ringo.lib.security import has_role
from ringo.lib.form import get_form_config
from ringo_printtemplate.model import Printable

disabled_actions_aspirant = {"aspirant": ["update", "delete"]}
disabled_actions_reviewer = {"reviewer": ["delete", "export", "import"]}

desc_s1 = """S1_DESC"""
desc_s2 = """S2_DESC"""
desc_s3 = """S3_DESC"""
desc_s4 = """S4_DESC"""
desc_s5 = """S5_DESC"""


def sendin_condition(item, transition):
    """Only application which are filled out completly can be sent in"""
    old_state = transition.get_start()
    request = old_state._statemachine._request
    formconfig = get_form_config(item, "update")
    form = Form(formconfig, item, dbsession=request.db)
    form.validate(None)
    return not (form.has_warnings() or form.has_errors())

def review_condition(item, transition):
    """Only reviewers are allowed to complete the review process."""
    old_state = transition.get_start()
    request = old_state._statemachine._request
    return has_role(request.user, "reviewer")

def accecpt_condtion(item, transition):
    return review_condition(item, transition)

def reject_condition(item, transition):
    return review_condition(item, transition)

def sendin_handler(item, transition):
    """Send a confirmation mail to the applicant."""
    old_state = transition.get_start()
    request = old_state._statemachine._request
    _ = request.translate
    # Send confirmation email to the owner of the application.
    recipients = [item.owner.profile[0].email]
    subject = _("Confirmation of receipt of application")
    template = "sendin_confirm_email"
    values = {"item": item}
    mail = Mail(recipients, subject, template, values)
    mailer = Mailer(request)
    mailer.send(mail)
    return item

def send_state_change_notification_email(item, transition, recipients=None):
    template = "statechange_notification_email"
    old_state = transition.get_start()
    new_state = transition.get_end()
    request = old_state._statemachine._request
    _ = request.translate
    values = {
            "ns": _(unicode(new_state)),
            "os": _(unicode(old_state)),
            "item": _(unicode(item)),
            "modul": _(unicode(get_item_modul(request, item).label)),
    }
    subject = _("State {modul} \"{item}\" changed: \"{os}\" -> \"{ns}\""
                .format(**values))
    if recipients:
        mail = Mail(recipients, subject, template, values)
        mailer = Mailer(request)
        mailer.send(mail)
    return item


class ApplicationStatemachine(Statemachine):

    def setup(self):
        _ = lambda x: x
        s1 = State(self, 1, _("In Progress"),
                   description=_(desc_s1),
                   disabled_actions=disabled_actions_reviewer)
        s2 = State(self, 2, _("In Review"),
                   description=_(desc_s2),
                   disabled_actions=disabled_actions_aspirant)
        s3 = State(self, 3, _("Reviewed"),
                   description=_(desc_s3),
                   disabled_actions=disabled_actions_aspirant)
        s4 = State(self, 4, _("Accepted"),
                   description=_(desc_s4),
                   disabled_actions=disabled_actions_aspirant)
        s5 = State(self, 5, _("Rejected"),
                   description=_(desc_s5),
                   disabled_actions=disabled_actions_aspirant)

        s1.add_transition(s2, _("Send in application"), sendin_handler,
                                                        sendin_condition)
        s2.add_transition(s3, _("Complete review"), handler, review_condition)
        s3.add_transition(s4, _("Accept application"), handler,
                          accecpt_condtion)
        s3.add_transition(s5, _("Reject application"), handler,
                          reject_condition)
        return s1


class ApplicationStateMixin(StateMixin):

    _statemachines = {"application_state_id": ApplicationStatemachine}

    application_state_id = sa.Column(sa.Integer, default=1)

    @property
    def application_state(self):
        state = self.get_statemachine('application_state_id')
        return state.get_state()


class Application(BaseItem, Printable, ApplicationStateMixin, Owned, Meta, Base):
    __tablename__ = 'applications'
    _modul_id = 1000
    _owner_cascades = "all, delete"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String, default="")

    ratings = sa.orm.relation("Rating", cascade="all, delete")

    @property
    def num_ratings(self):
        return len(self.ratings)
