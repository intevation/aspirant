"""Remove permission to list applications from applicants.

Revision ID: 9ae404fe1635
Revises: 79069c9d7af6
Create Date: 2016-06-01 12:02:27.312383

"""

# revision identifiers, used by Alembic.
revision = '9ae404fe1635'
down_revision = '79069c9d7af6'

from alembic import op
import sqlalchemy as sa


INSERTS = """
DELETE FROM nm_action_roles where rid = 3 and aid = 39;
"""
DELETES = """
INSERT into nm_action_roles (rid, aid) VALUES (3, 39);
"""


def iter_statements(stmts):
    for st in [x for x in stmts.split('\n') if x]:
        op.execute(st)


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    pass
    ### end Alembic commands ###
    iter_statements(INSERTS)


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    pass
    ### end Alembic commands ###
    iter_statements(DELETES)
