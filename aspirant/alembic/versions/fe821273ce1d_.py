"""empty message

Revision ID: fe821273ce1d
Revises: bf4318ff503f
Create Date: 2016-03-16 14:09:56.127442

"""

# revision identifiers, used by Alembic.
revision = 'fe821273ce1d'
down_revision = 'bf4318ff503f'

from alembic import op
import sqlalchemy as sa


INSERTS = """
INSERT INTO roles VALUES ('0fd0bc77e41d41269b18382a3bbc3e50', 3, 'Aspirant', 'aspirant', '', false, NULL, 1);
INSERT INTO roles VALUES ('6dd3a356cd5b48ac9970dfb12b28b9be', 4, 'Reviewer', 'reviewer', '', true, NULL, 1);
"""
DELETES = """"""


def iter_statements(stmts):
    for st in [x for x in stmts.split('\n') if x]:
        op.execute(st)


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    ### end Alembic commands ###
    iter_statements(INSERTS)


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    ### end Alembic commands ###
    iter_statements(DELETES)
