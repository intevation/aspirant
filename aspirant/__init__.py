#!/usr/bin/env python
# encoding: utf-8

from pyramid.config import Configurator
from pyramid.i18n import TranslationStringFactory

from ringo.lib.sql.db import setup_db_session, setup_db_engine
from ringo.model import Base
from ringo.config import setup_modules
from ringo.lib.i18n import translators
from ringo.lib.sitetree import site_tree_branches
from aspirant.model import extensions


sitetree = {
    "applications": {},
    "ratings": {"parent_site": "applications",
                "parent_item": "application",
                "display_item": "application",
                "display_format": u"Bewertung von Bewerbung „{item}“"}
}


site_tree_branches.append(sitetree)

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = setup_db_engine(settings)
    setup_db_session(engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    # Include basic ringo configuration.
    config.include('ringo')
    config.include('aspirant')
    for extension in extensions:
        config.include(extension)
    return config.make_wsgi_app()

def includeme(config):
    # Now configure the application and optionally overwrite previously
    translators.append(TranslationStringFactory('aspirant'))
    config.add_translation_dirs('aspirant:locale/')
    config.add_static_view('aspirant-static', path='aspirant:static',
                           cache_max_age=3600)
    config.commit()
    config.scan()
    config.commit()
