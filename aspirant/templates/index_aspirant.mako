## -*- coding:utf-8 -*-
<div class="row">
  % if not application:
    <div class="col-md-12">
      <p>Sie haben bislang noch <strong>keine</strong> Bewerbung angelegt. Wählen Sie „Bewerbung
      anlegen“, um eine neue Bewerbung anzulegen.</p>
      <a href="${request.route_path(h.get_action_routename(clazz, 'create'))}" class="btn btn-primary">Bewerbung anlegen</a>
    </div>
  % else:
    <div class="col-md-6">
      <p>Sie haben derzeit eine Bewerbung (${application}). Den aktuellen
        Status Ihrer Bewerbung und mögliche nächste Schritte finden sie
        nebenstehend unter „Status der Bewerbung“.
      <p>
      % if s.has_permission("update", application, request):
        <a href="${request.route_path(h.get_action_routename(application, 'update'), id=application.id)}" class="btn btn-primary">Bewerbung öffnen</a>
      % elif s.has_permission("read", application, request):
        <a href="${request.route_path(h.get_action_routename(application, 'read'), id=application.id)}" class="btn btn-primary">Bewerbung öffnen</a>
      % endif
      % if s.has_permission("delete", application, request):
        <a href="${request.route_path(h.get_action_routename(application, 'delete'), id=application.id, _query={'backurl': request.route_path('home')})}" class="btn btn-danger">Bewerbung löschen</a>
      % endif
      </p>
      <small>
        <ul class="list-unstyled">
          <li>Angelegt: ${h.prettify(request, application.created)}</li>
          <li>Zuletzt bearbeitet: ${h.prettify(request, application.updated)}</li>
        </ul>
      </small>
    </div>
    <div class="col-md-6">
      ${application_state_form}
    </div>
  % endif
</div>
<%include file="instructions.mako" />
