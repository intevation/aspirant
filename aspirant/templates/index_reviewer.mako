## -*- coding:utf-8 -*-
<div class="row">
  % if not states:
    <div class="col-md-12">
      <p>Es liegen für Sie derzeit keine Bewerbungen zur Bearbeitung vor.</a>
    </div>
  % else:
    <div class="col-md-12">
      <p>Nachfolgend finden Sie eine Auflistung von Bewerbungen, die Ihre
        Bearbeitung benötigen. Die Bewerbungen sind nach dem Status der
        Bewerbung gruppiert.<br/>Eine vollständige Übersicht aller Bewerbungen
        finden Sie oben im Menü unter „Bewerbungen“.</p>
      % for state, apps in states.iteritems():
      <h3>${state}</h3>
      % if state._description:
        <p>${state.get_description(request.user)}</p>
      % endif
      ${apps}
      % endfor
    </div>
  % endif
</div>
