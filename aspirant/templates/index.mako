## -*- coding:utf-8 -*-
<%inherit file="/main.mako" />
<%
mapping={'app_title': h.get_app_title()}
%>
% if not request.user:
<div class="row">
  <div class="col-md-12">
    <p>Willkommen zum
      <strong>${request.registry.settings.get("app.description", _("Application server")).decode("utf-8")}</strong>.</p>
    <p>Hier können Sie am Bewerbungsverfahren teilnehmen. Dazu benötigen Sie einen Zugang.
      Sie müssen hierfür eine Anmeldekennung (Benutzername) und ein Passwort
      festlegen sowie Ihre E-Mail-Adresse angeben. Sie können hier einen <a
        href="${request.route_path('register_user')}">Zugang
        registrieren</a>.</p>
    <p>Sobald Sie einen Zugang registriert haben, erhalten Sie eine
      Bestätigungsmail an die von Ihnen eingegeben E-Mail-Adresse mit einem
      Link zur Aktivierung Ihres Zugangs. Nachdem Sie dem Link gefolgt sind,
      können Sie sich jederzeit mit Ihren Zugangsdaten anmelden.</p>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <h3>Anmelden</h3>
    <p>Wenn Sie schon einen Zugang haben, dann können Sie sich hier anmelden.</p>
    <p>Sie haben die Zugangsdaten für Ihre Anmeldung vergessen? Sie können ihr
      <a href="${request.route_path('forgot_password')}">Passwort zurücksetzen</a> lassen.</p>
    <a href="${request.route_path('login')}" class="btn btn-primary btn-large" title="${_('Login in the application')}">${_('Login')}</a>
  </div>
  <div class="col-md-6">
    <h3>Zugang registrieren</h3>
    <p>Sie haben noch keinen Zugang? Hier können Sie sich registrieren.</p>
    <a href="${request.route_path('register_user')}" class="btn btn-default btn-large" title="">Zugang registrieren</a>
  </div>
</div>
% else:
  <%include file="/logininfo.mako" />
  <h2>${_('Willkommen %s' % request.user.profile[0])}</h2>
  % if s.has_role(request.user, "aspirant"):
    <%include file="/index_aspirant.mako" />
  % elif s.has_role(request.user, "reviewer"):
    <%include file="/index_reviewer.mako" />
  % endif
% endif
