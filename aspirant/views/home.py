#!/usr/bin/env python
# encoding: utf-8
import logging
from collections import OrderedDict

from pyramid.view import view_config
from ringo.views.request import handle_history
from ringo.views.helpers import get_item_form, render_item_form
from ringo.views.request import handle_POST_request, handle_redirect_on_success
from ringo.lib.security import has_role
from ringo.lib.renderer.lists import StaticListRenderer
from ringo.model.base import get_item_list
from aspirant.model.application import Application

log = logging.getLogger(__name__)


def render_applications_per_state(request, clazz, user,
                                  included_states, filter_method):
    # Collect applications which needs attention by the current
    # reviewer per state.
    states = {}
    app2state = {}
    applications = get_item_list(request, clazz, request.user)
    # Apply filters to applications (e.g state, reviewer=
    for app in applications:
        if (filter_method is not None and
           not filter_method(request, app)):
            continue
        if app.application_state_id not in states:
            states[app.application_state_id] = app.application_state
            app2state[app.application_state_id] = []

        # Only show configured states. If no states are configured show
        # all.
        if included_states:
            if app.application_state_id in included_states:
                app2state[app.application_state_id].append(app)
        else:
            app2state[app.application_state_id].append(app)

    # Now render a static overview list for each state which has
    # applications
    result = {}
    for state, apps in app2state.iteritems():
        if len(apps) == 0:
            continue
        listing = get_item_list(request, Application, items=apps)
        listing.paginate(len(listing.items))
        renderer = StaticListRenderer(listing, tablename="blackboard")
        result[states[state]] = renderer.render(request)
    result = OrderedDict(sorted(result.items(), key=lambda t: t[0]._id))
    return result


def _index_view(request, clazz, included_states=None, filter_method=None,
                callback=None):
    handle_history(request)
    if included_states is None:
        included_states = []
    values = {}
    values["clazz"] = clazz
    if request.user and has_role(request.user, "aspirant"):
        application = None
        values["application_state_form"] = None
        try:
            applications = request.db.query(clazz).filter(clazz.uid == request.user.id)
            if applications.all():
                application = applications.first()
                # Load form for setting the statefield.
                request.context.__model__ = clazz
                request.context.item = application
                form = get_item_form('stateonly', request)

                if request.POST and handle_POST_request(form, request, callback=callback):
                    return handle_redirect_on_success(request, backurl=request.route_path("home"))

                values['_roles'] = [str(r.name) for r in request.user.roles]
                values["application_state_form"] = render_item_form(request,
                                                                    form,
                                                                    values=values)
            values["application"] = application

        except:
            log.exception("Error while retrieving applications of aspirant")

    if request.user and has_role(request.user, "reviewer"):
        values["states"] = render_applications_per_state(request,
                                                         clazz,
                                                         request.user,
                                                         included_states,
                                                         filter_method)
    return values


@view_config(route_name='home', renderer='/index.mako')
def index_view(request):
    return _index_view(request, Application)
