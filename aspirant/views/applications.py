#!/usr/bin/env python
# encoding: utf-8
from datetime import date

from aspirant.model.application import Application
from pyramid.view import view_config
from ringo.lib.helpers import get_action_routename, get_action_url, prettify
from ringo.lib.renderer import InfoDialogRenderer
from ringo.lib.security import has_role
from ringo.views.base import create, update, list_ as blist_
from ringo.views.request import get_item_from_request, get_return_value


def __add_item_to_request(request):
    """Copied from ringo.views.base.create. Needed to have a item
    available in the context"""
    clazz = request.context.__model__
    try:
        factory = clazz.get_item_factory(request)
    except TypeError:
        # Old version of get_item_factory method which does not take an
        # request parameter.
        factory = clazz.get_item_factory()
        factory._request = request
    # Only create a new item if there isn't already an item in the
    # request.context. This can happen if we define a custom view for
    # create where the item gets created before to set some default
    # values e.g (See create function of forms)
    if not request.context.item:
        request.context.item = factory.create(request.user, {})
    return request


@view_config(route_name=get_action_routename(Application, 'create'),
             renderer='/applications/create.mako',
             permission='create')
def create_(request, callback=None):
    _ = request.translate
    settings = request.registry.settings
    deadline = settings.get("app.modul.applications.deadline_create")
    request = __add_item_to_request(request)

    # Check if the apspirant already has a application. In this case
    # stop here as it is only allowed to have one application per
    # aspirant.
    applications = request.db.query(Application).filter(Application.uid == request.user.id).all()
    if len(applications) > 0 and has_role(request.user, 'aspirant'):
        # Show warning and abort.
        title = _("Only one application is allowed")
        body = _("It is not allowed to create more than one application "
                 "per aspirant. As you already have an application you can "
                 "not create another one.",
                 mapping={})
        renderer = InfoDialogRenderer(request, title, body)
        ok_url = get_action_url(request, Application, 'list')
        rvalues = {'clazz': Application}
        rvalues["dialog"] = renderer.render(ok_url)
        return rvalues
    if deadline:
        # Check if deadline is elapsed.
        ymd = map(int, deadline.split("-"))
        deadline = date(ymd[0], ymd[1], ymd[2])
        if date.today() > deadline and has_role(request.user, 'aspirant'):
            # Show warning and abort.
            title = _("Application deadline has expired")
            body = _("The Application deadline (${deadline}) has expired. "
                     "It is no longer possible to create new applications.",
                     mapping={"deadline": prettify(request, deadline)})
            renderer = InfoDialogRenderer(request, title, body)
            ok_url = get_action_url(request, Application, 'list')
            rvalues = {'clazz': Application}
            rvalues["dialog"] = renderer.render(ok_url)
            return rvalues
    return create(request, callback)


@view_config(route_name=get_action_routename(Application, 'update'),
             renderer='/applications/update.mako',
             permission='update')
def update_(request, callback=None, validators=None):
    _ = request.translate
    settings = request.registry.settings
    deadline = settings.get("app.modul.applications.deadline_update")
    if deadline:
        # Check if deadline is elapsed.
        ymd = map(int, deadline.split("-"))
        deadline = date(ymd[0], ymd[1], ymd[2])
        if date.today() > deadline and has_role(request.user, 'aspirant'):
            # Show warning and abort.
            title = _("Application deadline has expired")
            body = _("The Application deadline (${deadline}) has expired. "
                     "It is no longer possible to update and submit "
                     "applications. Click on 'OK' to open your application"
                     "in read mode",
                     mapping={"deadline": prettify(request, deadline)})
            renderer = InfoDialogRenderer(request, title, body)
            ok_url = get_action_url(request,
                                    get_item_from_request(request),
                                    'read')
            rvalues = get_return_value(request)
            rvalues["dialog"] = renderer.render(ok_url)
            return rvalues
    item = get_item_from_request(request)
    return update(request, callback, values=item.get_values(),
                  validators=validators)


@view_config(route_name=get_action_routename(Application, 'list'),
             renderer='/applications/list.mako',
             permission='list')
def list_(request):
    rvalue = blist_(request)
    rvalue["hideadd"] = False
    applications = request.db.query(Application).filter(Application.uid == request.user.id).all()
    if len(applications) > 0 and has_role(request.user, 'aspirant'):
        rvalue["hideadd"] = True
    return rvalue
