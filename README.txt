aspirant README
==================

Getting Started
---------------

- cd <directory containing this file>

- $venv/bin/python setup.py develop

- $venv/bin/aspirant-admin db init

- $venv/bin/pserve development.ini

Aspirant specific config variables
----------------------------------

 * `app.modul.applications.deadline_create` Defines the last day applications can be
   created. If not defined application can be always created.
 * `app.modul.applications.deadline_update` Defines the last day applications can be
   updated. If not defined application can be always updated.

